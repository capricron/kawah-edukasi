package org.kawah.edukasi.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class User {

    public Integer id;

    @NotBlank(message = "Name is required")
    public String name;

    @NotBlank(message = "Email is required")
    public String email;

    @NotNull(message = "Age is required")
    @Min(value = 0, message = "Age must be greater than 0")
    @Max(value = 100, message = "Age must be less than 100")
    public Integer age;

    @NotBlank(message = "adress is required")
    public String address;

    public User() {
    }

    public User(String name, String email, Integer age, String address) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.address = address;
    }

}
